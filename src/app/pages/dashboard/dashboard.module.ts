import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';

const COMPONENTS = [
  DashboardComponent
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [
    CommonModule
  ]
})
export class DashboardModule { }
