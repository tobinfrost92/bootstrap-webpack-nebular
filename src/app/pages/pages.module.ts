import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent} from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { BackofficeModule } from '../@backoffice/backoffice.module';

const PAGES_COMPONENTS = [
  PagesComponent
];

@NgModule({
  imports: [
    PagesRoutingModule,
    DashboardModule,
    BackofficeModule
   
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
  providers: [],
})
export class PagesModule {
}
