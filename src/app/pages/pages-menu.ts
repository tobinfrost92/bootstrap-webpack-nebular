import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Tableau de Bord',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'EDITORIAL',
    group: true,
  },
    {
        title: 'Articles',
        icon: 'nb-compose'
    },{
        title: 'Medias',
        icon: 'nb-compose'
    },{
        title: 'Pages',
        icon: 'nb-compose'
    },{
        title: 'Commentaires',
        icon: 'nb-compose'
    },
    {
        title: 'CONFIGURATION',
        group: true,
    },
    {
        title: 'Apparence',
        icon: 'nb-compose'
    },{
        title: 'Utilisateurs',
        icon: 'nb-compose'
    },{
        title: 'Outils',
        icon: 'nb-compose'
    },{
        title: 'Réglages',
        icon: 'nb-compose'
    },
];
