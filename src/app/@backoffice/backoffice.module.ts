import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';


import {
  NbMenuModule,
  NbSidebarModule,
  NbThemeModule,
  NbDialogModule,
  NbWindowModule,
  NbToastrModule,
  NbLayoutModule,
  NbActionsModule,
  NbUserModule,
  NbSearchModule
} from '@nebular/theme';
import { HeaderComponent, FooterComponent } from './components';

const BASE_MODULES = [
  CommonModule
];

const NB_MODULES = [
  NbMenuModule,
  NbSidebarModule,
  NbThemeModule,
  NbDialogModule,
  NbWindowModule,
  NbToastrModule,
  NbLayoutModule,
  NbActionsModule,
  NbUserModule,
  NbSearchModule,
];

const COMPONENTS = [
  HeaderComponent,
  FooterComponent
];

const NB_THEME_PROVIDERS = [
  ...NbThemeModule.forRoot().providers,
  ...NbSidebarModule.forRoot().providers,
  ...NbMenuModule.forRoot().providers,
  ...NbDialogModule.forRoot().providers,
  ...NbWindowModule.forRoot().providers,
  ...NbToastrModule.forRoot().providers,
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...BASE_MODULES, ...NB_MODULES],
  exports: [...BASE_MODULES, ...NB_MODULES, ...COMPONENTS],
})
export class BackofficeModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: BackofficeModule,
      providers: [...NB_THEME_PROVIDERS],
    };
  }
}
